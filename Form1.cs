﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _3TieredDemo.BLL;

// Presentation Layer
namespace _3TieredDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();            
        }
               
        private void button1_Click(object sender, EventArgs e)
        {
            PersonBLL pb = new PersonBLL();
            try
            {              
                String id = this.textBox1.Text;
                if (id == "")
                    this.dataGridView1.DataSource = pb.GetPersons();
                else
                    this.dataGridView1.DataSource = pb.GetPersons(Convert.ToInt16(id));                   
            }
            catch
            {
                MessageBox.Show("Cannot Load Data");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PersonBLL pb = new PersonBLL();
            try
            {
                this.dataGridView1.DataSource = pb.GetPersons();
            }
            catch {
                MessageBox.Show("Cannot Load Data.");
                throw;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            PersonBLL pb = new PersonBLL();
            try
            {
                pb.Save(this.dataGridView1.DataSource as DataTable);
            }
            catch
            {
                MessageBox.Show("Cannot Save Data.");
                throw;
            }
        }
    }
}
