﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

// Data Access Layer
namespace _3TieredDemo.DAL
{
    public class PersonDAL
    {

        public string ConString;
        
        
        public PersonDAL()
        {
            string database = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PersonDB.mdf");
            ConString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + database + ";Integrated Security=True;Connect Timeout=30";
        }

        // example to use SqlDataReader
        public DataTable Read()
        {
            SqlConnection con = new SqlConnection();
            DataTable dt = new DataTable();
            con.ConnectionString = ConString;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCommand cmd = new SqlCommand("select * from Persons", con);
            try
            {
                SqlDataReader rd = cmd.ExecuteReader();
                dt.Load(rd);
                return dt;
            }
            catch
            {
                throw;
            }
        }

        public DataTable Read(Int16 id)
        {
            SqlConnection con = new SqlConnection();
            DataTable dt = new DataTable();
            con.ConnectionString = ConString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("select * from Persons where PersonID= " + id + "", con);
            try
            {
                SqlDataReader rd = cmd.ExecuteReader();
                dt.Load(rd);
                return dt;
            }
            catch
            {
                throw;
            }
        }

        // example to use SqlDataAdaptor
        public void Update(DataTable dt)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            var adaptor = new SqlDataAdapter();
            adaptor.SelectCommand = new SqlCommand("SELECT * FROM [Persons]", con);

            var cbr = new SqlCommandBuilder(adaptor);
            cbr.GetUpdateCommand();
            cbr.GetDeleteCommand();
            cbr.GetInsertCommand();

            try
            {
                adaptor.Update(dt);
            }
            catch
            {
                throw;
            }
        }

    }
}
