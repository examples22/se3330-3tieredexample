﻿using System;
using System.Collections.Generic;
using System.Data;
using _3TieredDemo.DAL;

// Business Logic Layer
namespace _3TieredDemo.BLL
{
    public class PersonBLL
    {
        

        public DataTable GetPersons()
        {
            PersonDAL dal = new PersonDAL();
            try
            {
                return dal.Read();
            }
            catch
            {
                throw;
            }
        }

        public DataTable GetPersons(Int16 ID)
        {
            PersonDAL dal = new PersonDAL();
            try
            {
                return dal.Read(ID);
            }
            catch
            {
                throw;
            }
        }

        public void Save(DataTable dt)
        {
            PersonDAL dal = new PersonDAL();
            try 
            { 
                dal.Update(dt); 
            }
            catch 
            { 
                throw; 
            }
        }

    }

}
